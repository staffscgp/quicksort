﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


//From Chapter 8: Pro .NET 4 Parallel Programming in C#
namespace QuickSort
{
    class ParallelQuickSort<T> : ISort<T> where T : IComparable
    {
        public int MaxDepth { get; set; }
        public int MinBlockSize { get; set; }

        public ParallelQuickSort() : this(16, 10000)
        {
        }

        public ParallelQuickSort(int maxDepth, int minBlockSize)
        {
            MaxDepth = maxDepth;
            MinBlockSize = minBlockSize;
        }

        public void Sort(T[] data)
        {
            doSort(data, 0, data.Length - 1, 0, MaxDepth, MinBlockSize);
        }

        private void doSort(T[] data, int startIndex, int endIndex, int depth, int maxDepth, int minBlockSize)
        {
            if (startIndex < endIndex)
            {
                if (depth > maxDepth || endIndex - startIndex < minBlockSize)
                {
                    Array.Sort(data, startIndex, endIndex - startIndex + 1);
                }
                else
                {
                    int pivotIndex = partitionBlock(data, startIndex, endIndex);

                    Task leftTask = Task.Factory.StartNew(() =>
                    {
                        doSort(data, startIndex, pivotIndex - 1, depth + 1, maxDepth, minBlockSize);
                    });
                    Task rightTask = Task.Factory.StartNew(() =>
                    {
                        doSort(data, pivotIndex - 1, endIndex, depth + 1, maxDepth, minBlockSize);
                    });

                    Task.WaitAll(leftTask, rightTask);
                }
            }
        }

       private int partitionBlock(T[] data, int startIndex, int endIndex)
        {
            T pivot = data[startIndex];

            swapValues(data, startIndex, endIndex);

            int storeIndex = startIndex;

            for (int i = startIndex; i < endIndex; i++)
            {
                if (data[i].CompareTo(pivot) <= 0)
                {
                    swapValues(data, i, storeIndex);
                    storeIndex++;
                }
            }
            swapValues(data, storeIndex, endIndex);
            return storeIndex;
        }

        private void swapValues(T[] data, int firstIndex, int secondIndex)
        {
            T holder = data[firstIndex];
            data[firstIndex] = data[secondIndex];
            data[secondIndex] = holder;
        }
    }
}
